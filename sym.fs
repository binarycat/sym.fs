\ a lisp like symbol abstraction
\ performs string duplication

\ how many bits should be used to store the length of a symbol
[undefined] sym_cfg_len_bits [if] 16 constant sym_cfg_len_bits [then]
\ how many bytes should be allocated to store all symbols
[undefined] sym_cfg_space [if] 4000 constant sym_cfg_space [then]

\ exists to catch silly errors like allocating the entire width of a cell to store the length
cell 8 * sym_cfg_len_bits - 1 swap lshift 1- sym_cfg_space < [if]
  .( SYM_CFG_LEN_BITS is too large, not enough bits left over to store offset )
[then]

create _sym_space sym_cfg_space allot
variable _sym_used

1 sym_cfg_len_bits lshift 1- constant _sym_len_mask

: _sym_str _sym_space _sym_used @ ;

: _sym_available ( len -- bool )
  _sym_used @ + sym_cfg_space < ;

: _sym_pack ( caddr len -- sym )
  swap _sym_space - 
  sym_cfg_len_bits lshift or ;

: _sym_create ( caddr len -- sym )
  dup _sym_available 0= abort" out of symbol space!"
  _sym_str + swap 2dup 2>r \ caddr end len
  move 2r> dup _sym_used +!
  _sym_pack ;

\ convert a string to a symbol, allocating symbol-space if needed
\ WARNING: symbol space is never freed.  if you want to compare user input to a symbol, convert the symbol to a string with >SYM
: >sym ( caddr len -- sym )
  _sym_str 2over search if 2swap nip nip _sym_pack else 2drop _sym_create then ;
    
: sym> ( sym -- caddr u )
  dup sym_cfg_len_bits rshift _sym_space +
  swap _sym_len_mask and ;

: Y" [char] " parse >sym state @ if postpone literal then ;
