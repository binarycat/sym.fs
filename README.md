# sym.fs: lisp-like symbols for forth

advantages of symbols over strings:
1. a symbol only takes up 1 stack cell
2. comparing two symbols is O(1) instead of O(n) for strings
3. two identical symbols will be stored in the same memory (this saves memory, and is also what allows them to be compared so quickly)

disadvantages of symbols:
1. they are stored in a fixed-size memory region
2. converting from a symbol to a string is O(n*m)
3. memory allocated for the storage of a symbol is never freed
4. the maximum length of a symbol is less than that of a string, due to only taking up a single cell

with all this in mind, the ideal usecase for symbols is to store strings known at compile time

# system requirements
* reccomended at least 32 bit cells.  technically usable with less, but will require fine tuning of configuration paramaters.
* SEARCH from the string wordset
* [UNDEFINED], [IF], and [THEN] from the programming-tools wordset
* POSTPONE from the forth2012 core wordset

# stable API

## compile time configuration
these are constants that may be defined before `sym.fs` is loaded.
if they are not defined, `sym.fs` will define them with a default value.

### `SYM_CFG_SPACE`
this defines the total number of bytes that will be allocated for symbol-space.

default is 4000.  increase this if you get an "out of symbol space" error.

### `SYM_CFG_LEN_BITS`
this defines how many bits of a cell will be used to store a symbol's length.

default is 16.  64 bit systems may choose to change this to 32 in rare cases.

## words
words not listed here are considered internal and subject to change

### `Y"`
parses a symbol literal, analagous to `S"`.

when called in interpretation mode, it will return the symbol on the stack.

when called in compilation mode, it will compile the symbol into the current definition using LITERAL.

### `>SYM` ( caddr u -- sym )
O(n) convert a string to a symbol.

memory allocated by this function will never be freed, so be careful calling it at runtime.

### `SYM>` ( sym -- caddr u )
O(1) convert a symbol to a string.

memory returned from this word MUST NOT be modified or freed.


