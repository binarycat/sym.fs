require ./sym.fs
require test/ttester.fs

T{ S" foo" >sym S" bar" >sym = -> false }T
T{ Y" baz" Y" bar" = -> false }T
T{ S" foo" >sym Y" foo" = -> true }T
T{ Y" foobar" sym> S" foobar" compare -> 0 }T

bye
